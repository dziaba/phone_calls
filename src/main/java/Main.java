import simulation.Simulation;

/**
 * Please use below text commands in console while main is running:
 * current               - to print ongoing phone calls
 * history               - to print all historical phone calls including rejected ones (phone calls with ednTime=null)
 * history time n        - to print all historical phone calls including rejected ones which ended in the last n seconds
 *                         (seconds are imitating minutes in this application)
 * history last n        - to print last n historical phone calls
 * off                   - to switch off messages about calls (they can make difficult writing other commands...)
 * on                    - to switch on messages about calls
 */

public class Main {
    public static void main(String[] args) {
       Simulation s = new Simulation();
       s.start();
    }
}
