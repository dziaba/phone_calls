package simulation;

import phones.CallStation;
import phones.Phone;

import java.util.Random;

public class PhoneSimulation implements Runnable {
    private CallStation station = CallStation.getInstance();
    private Random r = new Random();
    private boolean isWorking = true;
    private static final int BOUND=50;

    public void setWorking(boolean working) {
        isWorking = working;
    }

    @Override
    public void run() {
        while (isWorking) {
            try {
                Thread.sleep(r.nextInt(BOUND)*100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int numberOfPhones = station.getPhones().size();

            Phone p1 = station.getPhones().get(r.nextInt(numberOfPhones));
            Phone p2 = station.getPhones().get(r.nextInt(numberOfPhones));
            while (!p1.isAvailable() || p1.equals(p2)) {
                p1 = station.getPhones().get(r.nextInt(numberOfPhones));
                p2 = station.getPhones().get(r.nextInt(numberOfPhones));
            }
            station.makeCall(p1, p2);
        }
    }
}
