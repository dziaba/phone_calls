package simulation;

import phones.CallStation;
import phones.interfaces_settings.DoNotPrintSetting;
import phones.interfaces_settings.PrintSetting;

import java.util.Scanner;

public class CommandMain implements Runnable {
    private CallStation station = CallStation.getInstance();
    private boolean isWorking = true;



    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (isWorking) {
            String command = scanner.nextLine();
            String[] commandArr = command.split(" ");
            if (command.equalsIgnoreCase("current")) {
                station.getPhoneCallsRegister().showCurrentPhoneCalls();
            } else if (commandArr[0].equalsIgnoreCase("history")) {
                if (commandArr.length == 1) {
                    station.getPhoneCallsRegister().showHistoryPhoneCalls();
                } else if (commandArr[1].equalsIgnoreCase("time")) {
                    station.getPhoneCallsRegister().showHistoryPhoneCalls(Long.valueOf(commandArr[2]));
                } else if (commandArr[1].equalsIgnoreCase("last")) {
                    station.getPhoneCallsRegister().showHistoryPhoneCalls(Integer.valueOf(commandArr[2]));
                }
            } else if (command.equalsIgnoreCase("off")){
                station.setMessageSetting(new DoNotPrintSetting());
            } else if (command.equalsIgnoreCase("on")){
                station.setMessageSetting(new PrintSetting());
            }
        }
    }
}
