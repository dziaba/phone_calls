package simulation;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Simulation {
    private ExecutorService service = Executors.newFixedThreadPool(2);


    public void start(){
        PhoneSimulation phoneSimulation = new PhoneSimulation();
        CommandMain commandMain = new CommandMain();
        service.execute(phoneSimulation);
        service.execute(commandMain);
    }

}
