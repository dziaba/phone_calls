package phones.interfaces_settings;

import phones.CallRequest;
import phones.PhoneCall;

public interface IMessageSetting {
    void callStarted(CallRequest callRequest);
    void numberBusy(CallRequest callRequest);
    void callEnded(PhoneCall phoneCall);
}
