package phones.interfaces_settings;

import phones.CallRequest;
import phones.PhoneCall;
import phones.interfaces_settings.IMessageSetting;

public class DoNotPrintSetting implements IMessageSetting {
    @Override
    public void callStarted(CallRequest callRequest) {

    }

    @Override
    public void numberBusy(CallRequest callRequest) {

    }

    @Override
    public void callEnded(PhoneCall phoneCall) {

    }
}
