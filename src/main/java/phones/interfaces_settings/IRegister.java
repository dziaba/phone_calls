package phones.interfaces_settings;

public interface IRegister {
    void showCurrentPhoneCalls();
    void showHistoryPhoneCalls();
    void showHistoryPhoneCalls(long seconds);
    void showHistoryPhoneCalls(int number);
}
