package phones.interfaces_settings;

import phones.CallRequest;
import phones.PhoneCall;
import phones.interfaces_settings.IMessageSetting;

public class PrintSetting implements IMessageSetting {
    @Override
    public void callStarted(CallRequest callRequest) {
        System.out.println(String.format("A call started between numbers %s and %s",callRequest.getFromPhone().getNumber(),
                callRequest.getToPhone().getNumber()));
    }

    @Override
    public void numberBusy(CallRequest callRequest) {
        System.out.println(String.format("%s calling to %s - number is busy/unavailable right now, please try again later",
                callRequest.getFromPhone().getNumber(),callRequest.getToPhone().getNumber()));
    }

    @Override
    public void callEnded(PhoneCall phoneCall) {
        System.out.println(String.format("The call between numbers %s and %s has ended", phoneCall.getFromPhone().getNumber(),
                phoneCall.getToPhone().getNumber()));
    }
}
