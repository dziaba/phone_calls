package phones;

import phones.interfaces_settings.IMessageSetting;
import phones.interfaces_settings.IRegister;
import phones.interfaces_settings.PrintSetting;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CallStation implements Observer {
    private static CallStation instance = null;

    private ExecutorService service = Executors.newFixedThreadPool(5);
    private Map<Integer, Phone> phones = new HashMap<>();
    private PhoneCallsRegister register;
    private IMessageSetting messageSetting;
    private static final int NUMBER_OF_PHONES = 10;

    private CallStation() {
        register = new PhoneCallsRegister();
        messageSetting = new PrintSetting();

        for (int i=0; i<NUMBER_OF_PHONES; i++){
            Phone phone = new Phone("User"+i, 100+i);
            phones.put(i, phone);
        }
    }

    public static CallStation getInstance(){
        if (instance==null){
            instance=new CallStation();
        }
        return instance;
    }

    public Map<Integer, Phone> getPhones() {
        return phones;
    }

    public IRegister getPhoneCallsRegister(){
        return register;
    }

    public void setMessageSetting(IMessageSetting messageSetting) {
        this.messageSetting = messageSetting;
    }

    public void makeCall(Phone fromPhone, Phone toPhone){
        fromPhone.setAvailable(false);
        CallRequest request = new CallRequest(fromPhone, toPhone);
        request.addObserver(this);
        service.execute(request);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof CallRequest){
            CallRequest callRequest = (CallRequest)o;
            boolean reached = (boolean)arg;
            PhoneCall phoneCall = new PhoneCall(callRequest.getFromPhone(), callRequest.getToPhone());

            if (reached){
                phoneCall.addObserver(this);
                service.execute(phoneCall);
                register.addToCurrent(phoneCall);

                messageSetting.callStarted(callRequest);
            }else{
                callRequest.getFromPhone().setAvailable(true);
                register.addRejected(phoneCall);

                messageSetting.numberBusy(callRequest);
            }

        }else if (o instanceof PhoneCall){
            PhoneCall phoneCall = (PhoneCall)o;
            register.moveFromCurrentToHistory(phoneCall);

            messageSetting.callEnded(phoneCall);
        }
    }

}
