package phones;

import java.util.Observable;

public class CallRequest extends Observable implements Runnable {
    private Phone fromPhone;
    private Phone toPhone;

    public CallRequest(Phone fromPhone, Phone toPhone) {
        this.fromPhone = fromPhone;
        this.toPhone = toPhone;
    }

    public Phone getFromPhone() {
        return fromPhone;
    }

    public Phone getToPhone() {
        return toPhone;
    }

    @Override
    public void run() {
        boolean reached;
        if (toPhone.isAvailable()){
            toPhone.setAvailable(false);
            reached = true;
        }else{
            reached = false;
        }
        setChanged();
        notifyObservers(reached);
    }

}
