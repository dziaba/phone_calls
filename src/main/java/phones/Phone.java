package phones;

public class Phone {

    private String name;
    private int number;
    private boolean available;

    public Phone(String name, int number) {
        this.name = name;
        this.number = number;
        this.available = true;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
