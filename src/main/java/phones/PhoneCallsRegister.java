package phones;

import phones.interfaces_settings.IRegister;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhoneCallsRegister implements IRegister {
    private List<PhoneCall>  historyPhoneCalls;
    private List<PhoneCall> currentPhoneCalls;

    public PhoneCallsRegister() {
        historyPhoneCalls = Collections.synchronizedList(new ArrayList<>());
        currentPhoneCalls = Collections.synchronizedList(new ArrayList<>());
    }

    public void addToCurrent(PhoneCall phoneCall){
            currentPhoneCalls.add(phoneCall);
    }

    public void addRejected(PhoneCall phoneCall){
            historyPhoneCalls.add(phoneCall);
    }

    public void moveFromCurrentToHistory(PhoneCall phoneCall){
            currentPhoneCalls.remove(phoneCall);
            historyPhoneCalls.add(phoneCall);
    }

    public void showCurrentPhoneCalls(){
        synchronized (currentPhoneCalls) {
            currentPhoneCalls.stream().forEach(System.out::println);
        }
    }

    public void showHistoryPhoneCalls(){
        synchronized (historyPhoneCalls) {
            historyPhoneCalls.stream().forEach(System.out::println);
        }
    }

    public void showHistoryPhoneCalls(long seconds){
        synchronized (historyPhoneCalls) {
            historyPhoneCalls.stream().filter(x -> x.getEndTime()!=null).filter(x -> x.getEndTime().isAfter(LocalDateTime.now().minusSeconds(seconds)))
                    .forEach(System.out::println);
        }
    }

    public void showHistoryPhoneCalls(int number){
        synchronized (historyPhoneCalls) {
            historyPhoneCalls.stream().skip(Math.max(0, historyPhoneCalls.size() - number)).forEach(System.out::println);
        }
    }


}
