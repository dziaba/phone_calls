package phones;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Observable;
import java.util.Random;

public class PhoneCall extends Observable implements Runnable {
    private Phone fromPhone;
    private Phone toPhone;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private long duration;
    private static Random r = new Random();
    private static final int BOUND=20;

    public PhoneCall(Phone fromPhone, Phone toPhone) {
        this.fromPhone = fromPhone;
        this.toPhone = toPhone;
        startTime = LocalDateTime.now();
        duration = 0;
    }

    public Phone getFromPhone() {
        return fromPhone;
    }

    public Phone getToPhone() {
        return toPhone;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public void run() {
        long time = r.nextInt(BOUND)*1000;
        try{
            Thread.sleep(time);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        fromPhone.setAvailable(true);
        toPhone.setAvailable(true);
        endTime = LocalDateTime.now();
        duration = Duration.between(startTime, endTime).getSeconds();

        setChanged();
        notifyObservers(this);
    }

    @Override
    public String toString() {
        return "PhoneCall{" +
                "fromNumber=" + fromPhone.getNumber() +
                ", toNumber=" + toPhone.getNumber() +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                '}';
    }
}
