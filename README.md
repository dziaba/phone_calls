
This is an application simulating phones calling randomly to each other:

- every phone calls another one (randomly, all phones know each other) from time to time;
- calls are started immediately if number, which is is being called to, is available;
- calls (also rejected ones) are stored in PhoneCallsRegister from which current and historical calls can be checked at any time while application is running;
- all cammands which can be used in console are described in Main class of the application.

To check it out:
- clone the repository
- open project via pom.xml

